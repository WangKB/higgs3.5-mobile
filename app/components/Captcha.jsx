import React from 'react';
export class Captcha extends React.Component {
    constructor(props) {
        super(props);
        this.renderCode = this.renderCode.bind(this);
    }
    componentWillMount(){
        this.renderCode();
    }

    renderCode(){
        var timestamp=new Date().getTime();
        this.setState({v:timestamp});
    }


    render() {
        var url="/jcaptcha.jpg?v="+this.state.v;
        return (
        <div className="captcha">
            <img src={url}
            onClick={this.renderCode}></img>
        </div>
        );
    }
}
