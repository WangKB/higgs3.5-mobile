import React from 'react';
import { Link } from 'react-router'
import { Form, Icon, Input, Button, Checkbox,Row,Col } from 'antd';
const FormItem = Form.Item;
import LogoImg from '../img/logo.png';

class NormalLoginForm extends React.Component {
  constructor(props) {
        super(props);
        this.handleSubmit=this.handleSubmit.bind(this);
    }


  handleSubmit(e){
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
      }
    });
  }
  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <Form onSubmit={this.handleSubmit} className="login-form">
          <Row className="logo-row">
            <Col span={24}>
                <img className="img-logo" src={LogoImg}/>
            </Col>
         </Row>
        <FormItem>
          {getFieldDecorator('userName', {
            rules: [{ required: true, message: 'Please input your username!' }],
          })(
            <Input prefix={<Icon type="user" style={{ fontSize: 13 }} />} placeholder="Username" />
          )}
        </FormItem>
        <FormItem>
          {getFieldDecorator('password', {
            rules: [{ required: true, message: 'Please input your Password!' }],
          })(
            <Input prefix={<Icon type="lock" style={{ fontSize: 13 }} />} type="password" placeholder="Password" />
          )}
        </FormItem>
        <Row className="input-row">
            <Col span={24}>
                <Button type="primary" htmlType="submit" className="button" size="large">登录</Button>
            </Col>
        </Row>
        <Row className="input-row">
            <u><Link to="/register">去注册</Link></u>
        </Row>
      </Form>
    );
  }
}

const Login = Form.create()(NormalLoginForm);

export default Login;