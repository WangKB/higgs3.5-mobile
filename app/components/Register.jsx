import React from 'react';
import { Link } from 'react-router'
import { Form, Input, Tooltip, Icon, Cascader, Select, Row, Col, Checkbox, Button } from 'antd';

const FormItem = Form.Item;
const Option = Select.Option;

const residences = [{
  value: 'zhejiang',
  label: '浙江',
  children: [{
    value: 'hangzhou',
    label: '杭州',
  }],
}, {
  value: 'jiangsu',
  label: '江苏',
  children: [{
    value: 'nanjing',
    label: '南京',
  }],
}];

class RegistrationForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            confirmDirty: false,
        };
        this.handleSubmit=this.handleSubmit.bind(this);
        this.handleConfirmBlur=this.handleConfirmBlur.bind(this);
        this.checkConfirm=this.checkConfirm.bind(this);
        this.checkPassword=this.checkPassword.bind(this);
    }
    
  handleSubmit(e) {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        this.props.history.push('/login');
        console.log('Received values of form: ', values);
      }
    });
  }
  handleConfirmBlur(e){
    const value = e.target.value;
    this.setState({ confirmDirty: this.state.confirmDirty || !!value });
  }
  checkPassword(rule, value, callback){
    const form = this.props.form;
    if (value && value !== form.getFieldValue('password')) {
      callback('两次密码输入不一致！');
    } else {
      callback();
    }
  }
  checkConfirm(rule, value, callback){
    const form = this.props.form;
    if (value && this.state.confirmDirty) {
      form.validateFields(['confirm'], { force: true });
    }
    callback();
  }
  render() {
    const { getFieldDecorator } = this.props.form;
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 4 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 18 },
      },
    };
    const tailFormItemLayout = {
      wrapperCol: {
        xs: {
          span: 24,
          offset: 0,
        },
        sm: {
          span: 14,
          offset: 6,
        },
      },
    };
    const prefixSelector = getFieldDecorator('prefix', {
      initialValue: '86',
    })(
      <Select className="icp-selector">
        <Option value="86">+86</Option>
      </Select>
    );
    return (
      <Form onSubmit={this.handleSubmit}>
        {/*<Row className="logo-row">
                    <Col span={24}>
                        <img className="img-logo" src={LogoImg}/>
                    </Col>
         </Row>*/}
         <FormItem
          {...formItemLayout}
          label="用户名"
          hasFeedback
        >
          {getFieldDecorator('nickname', {
            rules: [
            { required: true, message: '请输入您的用户名！', whitespace: true },
            {max:40,message:'用户名过长！'}],
          })(
            <Input />
          )}
        </FormItem>
        <FormItem
          {...formItemLayout}
          label="邮箱"
          hasFeedback
        >
          {getFieldDecorator('email', {
            rules: [{
              type: 'email', message: '请输入真实邮箱！',
            }, {
              required: true, message: '请输入邮箱！',
            }],
          })(
            <Input />
          )}
        </FormItem>
        <FormItem
          {...formItemLayout}
          label="手机号码"
          hasFeedback
        >
          {getFieldDecorator('phone', {
            rules: [{ required: true, message: '请输入您的手机号码！' },
            {len:11,message:'请输入真实手机号码！'}],
          })(
            <Input addonBefore={prefixSelector} />
          )}
        </FormItem>
        <FormItem
          {...formItemLayout}
          label="公司名称"
          hasFeedback
        >
          {getFieldDecorator('componyname', {
            rules: [{ required: true, message: '请输入您的公司名称！', whitespace: true },
            {max:50,message:'公司名过长！'}],
          })(
            <Input />
          )}
        </FormItem>
        <FormItem
          {...formItemLayout}
          label="密码"
          hasFeedback
        >
          {getFieldDecorator('password', {
            rules: [{
              required: true, message: '请输入密码！',
            }, {
              validator: this.checkConfirm,
            }],
          })(
            <Input type="password" />
          )}
        </FormItem>
        <FormItem
          {...formItemLayout}
          label="确认密码"
          hasFeedback
        >
          {getFieldDecorator('confirm', {
            rules: [{
              required: true, message: '请确认您的密码！',
            }, {
              validator: this.checkPassword,
            }],
          })(
            <Input type="password" onBlur={this.handleConfirmBlur} />
          )}
        </FormItem>
        
        {/*<FormItem
          {...formItemLayout}
          label="选择城市"
        >
          {getFieldDecorator('residence', {
            initialValue: ['zhejiang', 'hangzhou'],
            rules: [{ type: 'array', required: true, message: '请选择你的城市！' }],
          })(
            <Cascader options={residences} />
          )}
        </FormItem>
        
        <FormItem {...tailFormItemLayout} style={{ marginBottom: 8 }}>
          {getFieldDecorator('agreement', {
            valuePropName: 'checked',
          })(
            <Checkbox>我同意<a>《浩格云信查询系统服务条款》</a></Checkbox>
          )}
        </FormItem>*/}
        <Row className="input-row">
            <Col span={24}>
                <Button type="primary" htmlType="submit" className="button" size="large">注册</Button>
            </Col>
        </Row>
        <Row className="input-row">
            <u><Link to="/">回到登录</Link></u>
        </Row>
      </Form>
    );
  }
}

const Register = Form.create()(RegistrationForm);
export default Register;
