import React from 'react'
import { Router, Route, hashHistory } from 'react-router'
import Index from "./components/Index.jsx";
import Login from "./components/Login.jsx";
import Register from "./components/Register.jsx";

const Routers = () => (
    <Router history={hashHistory}>
        <Route path="/" component={Login}/>
        <Route path="/login" component={Login}/>
        <Route path="/register" component={Register}/>
    </Router>
)


export default Routers;
