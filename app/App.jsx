import Routers from './AppRouters.jsx';
import React from 'react'
import ReactDOM from 'react-dom'
import './sass/app.scss'


class App extends React.Component{
    constructor() {
        super();
    }
    render() {
        return (
            <Routers/>
        )
    }
};

ReactDOM.render(<App />, document.getElementById('app'));